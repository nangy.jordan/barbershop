let nav = document.getElementById('navigation');
let lien = document.getElementsByClassName('lienNav');
let icon = document.getElementById('icon');

window.onresize = colorHeader;
window.onload = colorHeader;

function colorHeader() 
{

    let myWidth = window.innerWidth;

    window.onscroll = function () 
    {

        if (myWidth > 768) 
        {

            if (document.documentElement.scrollTop > 980)
            {
                nav.style.background = "#373737";
                lien[0].style.color = "white";
                lien[1].style.color = "white";
                lien[2].style.color = "white";
                 lien[3].style.color = "white";
            }
            else 
            {
                nav.style.background = "#F0EAd6";
                lien[0].style.color = "#373737";
                lien[1].style.color = "#373737";
                lien[2].style.color = "#373737";
                lien[3].style.color = "#373737";
            }

        }

        else
        {
            nav.style.background = "#F0EAd6";
            lien[0].style.color = "#373737";
            lien[1].style.color = "#373737";
            lien[2].style.color = "#373737";
            lien[3].style.color = "#373737";

        }

    }

}

// Animation du hamburger rajoute une class pour le responsive a edit en css//
function toggleNav() {
    if(nav.className === "")
    {
        nav.className += "responsive";
    }
    else{
        nav.className = "";
    }
}

//Lorsque je clique autre part sur la page le menu burger se ferme//
$('html').click(function (){

    if(nav.className += "responsive")
    {
        nav.className = "";
    }
})

//mais si je click sur le burger sa marche
$('#icon').click(function(event){

    event.stopPropagation();
});


//Smooth scroll


//Animation sur le click de l'icon ciseau//
$('#arrow').click(function () {

    $('html,body').animate({
        scrollTop: $('#section-about').offset().top -50
    },
    'slow')

})

//Lie les liens du Nav avec un anim//
$('.lienNav:nth-child(1)').click(function () {

    $('html,body').animate({
        scrollTop: $('#accueil').offset().top +50
    },
    'slow')

})

$('.lienNav:nth-child(2)').click(function () {

    $('html,body').animate({
        scrollTop: $('#section-about').offset().top -50
    },
    'slow')

})

$('.lienNav:nth-child(3)').click(function () {

    $('html,body').animate({
        scrollTop: $('#tarifs').offset().top -50
    },
    'slow')

})

$('lienNav:nth-child(4)').click(function () {

    $('html,body').animate({
        scrollTop: $('#contact').offset().top -50
    },
    'slow')

})




